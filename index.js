const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const request = require('request');
const apiaiApp = require('apiai')("7b89647fa6e6409b9c7f193f74a0baf9");
const verify_token = process.env.verify_token;
const token = process.env.token
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
    res.status(200).send("hello on bot");
});


app.get('/webhook', function (req, res) {
  if (req.query['hub.mode'] === 'subscribe' && 
		req.query['hub.verify_token'] === verify_token) {
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);
  }
});


/* Handling all messenges */
app.post('/webhook', function (req, res) {
	
  var data = req.body;
  if (data.object == 'page') {
    data.entry.forEach(function (pageEntry) {
      var pageID = pageEntry.id;
      var timeOfEvent = pageEntry.time;
      pageEntry.messaging.forEach(function (event) {
        if (event.message && event.message.text) {
          sendMessage(event);
        }
      });
    });
    res.sendStatus(200);
  }
});

app.post('/ai', (req, res) => {
    let city = req.body.result.parameters['geo-city'];
    let date = req.body.result.parameters['date'];
    console.log("date = " + typeof(date))
    if  (undefined !== date && "" !== date) {
       handleDate(city, date).then(function (value) {
         console.log("value = ")
         console.log(value)
            return res.status(200).json(value);
        })
    } else {
      let restUrl = 'http://api.openweathermap.org/data/2.5/weather?APPID='+ "ed0062b9f79103fa9d83e060c175b198" +'&q='+city + '&units=metric&lang=fr';
    request.get(restUrl, (err, response, body) => {
      if (!err && response.statusCode == 200) {
        let json = JSON.parse(body);
        let msg = json.weather[0].description + ', ' + json.main.temp + ' °C';
        return res.json({
          speech: msg,
          displayText: msg,
          source: 'weather'});
      } else {
        return res.status(400).json({
          status: {
            code: 400,
            errorType: 'I failed to look up the city name.'}});
      }})
    }
    
});

function sendMessage(event) {
  let sender = event.sender.id;
  let text = event.message.text;
  let apiai = apiaiApp.textRequest(text, {
    sessionId: "toto"
  });

  apiai.on('response', (response) => {
    console.log(response)
    let aiText = response.result.fulfillment.speech;

    request({
      url: 'https://graph.facebook.com/v2.6/me/messages',
      qs: {access_token: token},
      method: 'POST',
      json: {
        recipient: {id: sender},
        message: {text: aiText}
      }
    }, (error, response) => {
      if (error) {
          console.log('Error sending message: ', error);
      } else if (response.body.error) {
          console.log('Error: ', response.body.error);
      }
    });
  });

  apiai.on('error', (error) => {
    console.log(error);
  });

  apiai.end();
}

function handleDate(city, date){
  return new Promise((resolve, reject) => {
     let restUrl = 'http://api.openweathermap.org/data/2.5/forecast?APPID='+ "ed0062b9f79103fa9d83e060c175b198" +'&q='+city + '&units=metric&lang=fr';
     let msg = "";
     let values = [];
     request.get(restUrl, (err, response, body) => {
      if (!err && response.statusCode == 200) {
        let json = JSON.parse(body)["list"];
        json.forEach(function(element) {
          if  (element['dt_txt'].split(' ')[0] === date) {
            values.push(element);
          }
        });

        values.forEach(function(element){
          msg += "A " + element.dt_txt.split(' ')[1] + " " + element.weather[0].description + ', ' + element.main.temp + ' °C \n';
        });

        resolve({
          speech: msg,
          displayText: msg,
          source: 'weather'
        })
      } else {
        return res.status(400).json({
          status: {
            code: 400,
            errorType: 'I failed to look up the city name.'}});
      }})
  });
}

function handleDate(city, date){
  return new Promise((resolve, reject) => {
     let restUrl = 'http://api.openweathermap.org/data/2.5/forecast?APPID='+ "ed0062b9f79103fa9d83e060c175b198" +'&q='+city + '&units=metric&lang=fr';
     let msg = "";
     let values = [];
     request.get(restUrl, (err, response, body) => {
      if (!err && response.statusCode == 200) {
        let json = JSON.parse(body)["list"];
        json.forEach(function(element) {
          if  (element['dt_txt'].split(' ')[0] === date) {
            values.push(element);
          }
        });

        values.forEach(function(element){
          msg += "A " + element.dt_txt.split(' ')[1] + " " + element.weather[0].description + ', ' + element.main.temp + ' °C \n';
        });

        resolve({
          speech: msg,
          displayText: msg,
          source: 'weather'
        })
      } else {
        return res.status(400).json({
          status: {
            code: 400,
            errorType: 'I failed to look up the city name.'}});
      }})
  });
}



const server = app.listen(process.env.PORT || 5000, () => {
  console.log('Express server listening on port %d in %s mode', server.address().port, app.settings.env);
});